# usbguard-notifier

Simply lets you either allow or block a USB device when it is inserted.

![](example.png)

## Dependency

* `usbguard`
* `zenity` or [`qarma`](https://github.com/luebking/qarma)



## Installation

```bash
chmod +x usbguard-notifier.sh
sudo cp usbguard-notifier.sh /usr/local/bin
```

If you want it to run on startup, you can add it as a service:

```bash
sudo tee /etc/systemd/system/usbguard-notifier.service << EOF
[Unit]
After=usbguard.service

[Service]
ExecStart=/usr/local/bin/usbguard-notifier.sh

[Install]
WantedBy=default.target
EOF
```

```bash
sudo systemctl enable --now usbguard-notifier.service
```

