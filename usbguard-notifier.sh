#!/usr/bin/env sh

if command -v qarma >/dev/null 2>&1; then
    notify='qarma'
else
    notify='zenity'
fi

# CONSTANTS
inserted='Insert'
ejected='Remove'

# CONTROL VARIABLES
event_type=''
device_id=''
status=''

usbguard watch | {
    while :; do
	read -r event

	if echo "$event" | grep -iq 'id='; then
	    device_id=$(echo "$event" | grep -o '[0-9]*')
	fi

	if echo "$event" | grep -iq '\[device\]'; then
	    status="$(echo "$event" | awk '{print $2}')"
	fi

	if echo "$event" | grep -iq "event=$inserted"; then
	    event_type=$inserted
	fi

	if echo "$event" | grep -iq "event=$ejected"; then
	    event_type=$ejected
	fi

	if echo "$event" | grep -iq 'device_rule' \
	    && [ "$event_type" = "$inserted" ]  \
	    && [ "$status" = "PresenceChanged:" ]; then
	    
	    device_name=$(echo "$event" | awk -F\" '{print $4}')
	    if export DISPLAY=:0 && $notify --question --text "Do you wish to allow $device_name?" --no-wrap --ok-label "Allow" --cancel-label "Block"; then
		sudo usbguard allow-device "$device_id"
	    else
		sudo usbguard block-device "$device_id"
	    fi
	fi

    done
}
